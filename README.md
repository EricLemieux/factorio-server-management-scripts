# Factorio server management scripts

Ease of life scripts to help with management of a Factorio server on a basic Digital Ocean droplet.

Assumptions:

* SSH is already configured to the server
* You are running as root on the server
* Your factorio configs live in a folder called `factorio`, ie using [this docker image](https://hub.docker.com/r/factoriotools/factorio)

## Backups

`backup.sh` will create a tarball of the factorio folder on the server and download a copy to your local system.
