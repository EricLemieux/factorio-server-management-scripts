#!/bin/bash

set -e

ip=${1}
date=$(date +%F-%H-%M)
remote_user="root"
backup="factorio_${date}.tar.gz"

function br() {
  echo ""
  echo "----------"
  echo ""
}

if [[ -z "${ip}" ]]
then
  echo "IP address is missing"
  exit
fi

br
echo "Creating backup on remote server"
ssh ${remote_user}@${ip} tar czf ${backup} factorio

br
echo "Copying backup to local"
scp ${remote_user}@${ip}:${backup} ${backup}

br
echo "Validating that the backup was created locally"
if [[ -f ${backup} ]]
then
  br
  echo "Removing backup from the remote server"
  ssh ${remote_user}@${ip} rm ${backup}
else
  br
  echo "Backup was not found locally, a human should look into this"
  exit
fi

